import path from "path";
import { TaskCode, alignmentSuffix, phylotreeSuffix, sequenceSuffix } from "../../common/constants";
import { WorkflowValidator } from "../../ports/core";
import { Task, Workflow } from "../../ports/models";

// 系统发育分析工作流校验器
export class PhylogenyAnalysisWorkflowValidator implements WorkflowValidator {
    // 类属性挂载单例
    private static readonly validator: WorkflowValidator = new PhylogenyAnalysisWorkflowValidator();
    // 构造函数设置为私有不允许外部实例化
    private constructor() { }
    // 类方法获取单例
    static get instance(): WorkflowValidator {
        return this.validator;
    }
    // 校验工作流是否合法
    public validate(workflow: Workflow): Error | null {
        // 不存在输入文件时报错
        if (workflow.entries.length === 0) {
            return new Error('no input file specified');
        }
        // 不存在分析任务时报错
        if (workflow.tasks.length === 0) {
            return new Error('no task in workflow');
        }
        // 检查输入文件格式是否符合工作流中第一个任务的要求
        const err = this.checkInputFileFormat(workflow.tasks[0].code, workflow.entries);
        if (err !== null) {
            return err;
        }
        // 检查工作流中任务流程是否合法
        const taskFlow: TaskCode[] = workflow.tasks.map((task: Task) => task.code);
        if (!this.isTaskFlowValid(taskFlow)) {
            return new Error('invalid workflow');
        }
        return null;
    }
    // 检查输入文件格式
    private checkInputFileFormat(task: TaskCode, inputFiles: string[]): Error | null {
        // 当任务为多序列对齐/序列拼接时，输入只能是FASTA格式序列文件
        if ([TaskCode.MAFFT, TaskCode.MACSE, TaskCode.SEQUENCE_CONCATENATOR].includes(task)) {
            for (const file of inputFiles) {
                if (!sequenceSuffix.includes(path.extname(file))) {
                    return new Error(`invalid input file [${file}], it must be in FASTA format!`);
                }
            }
        }
        // 当任务为合并进化树时，输入只能是NEWICK格式进化树文件
        if (task === TaskCode.ASTRAL) {
            for (const file of inputFiles) {
                if (!phylotreeSuffix.includes(path.extname(file))) {
                    return new Error(`invalid input file [${file}], it must be in NEWICK format!`);
                }
            }
        }
        // 当任务为对齐格式转换/构建进化树/对齐序列优化/模型搜索时，输入只能是合法的多序列对齐文件
        if ([TaskCode.ALTER, TaskCode.TRIMAL, TaskCode.MODEL_FINDER, TaskCode.IQTREE, TaskCode.MRBAYES]) {
            for (const file of inputFiles) {
                if (!alignmentSuffix.includes(path.extname(file))) {
                    return new Error(`invalid input file [${file}], it must be in FASTA/PHYLIP/MEGA/NEXUS format!`);
                }
            }
        }
        return null;
    }
    // 检查任务流是否合法
    private isTaskFlowValid(taskFlow: TaskCode[]): boolean {
        // 任务数少于2个时必定合法
        if (taskFlow.length < 2) {
            return true;
        }
        // 取出第一个任务
        const task = taskFlow.shift();
        // 根据当前任务判断下一个任务是否合法
        let validNextTask: TaskCode[] = [];
        switch (task) {
            // 对齐格式转换下一步可以是对齐序列优化/模型搜索/序列拼接/构建进化树
            case TaskCode.ALTER:
                validNextTask = [TaskCode.TRIMAL, TaskCode.MODEL_FINDER, TaskCode.SEQUENCE_CONCATENATOR, TaskCode.IQTREE, TaskCode.MRBAYES];
                break;
            // 多序列对齐下一步可以是对齐序列优化/模型搜索/序列拼接/构建进化树/对齐格式转换
            case TaskCode.MAFFT, TaskCode.MACSE:
                validNextTask = [TaskCode.TRIMAL, TaskCode.MODEL_FINDER, TaskCode.SEQUENCE_CONCATENATOR, TaskCode.IQTREE, TaskCode.MRBAYES, TaskCode.ALTER];
                break;
            // 对齐序列优化下一步可以是模型搜索/序列拼接/构建进化树/对齐格式转换
            case TaskCode.TRIMAL:
                validNextTask = [TaskCode.MODEL_FINDER, TaskCode.SEQUENCE_CONCATENATOR, TaskCode.IQTREE, TaskCode.MRBAYES, TaskCode.ALTER];
                break;
            // 模型搜索下一步只能是多序列对齐
            case TaskCode.MODEL_FINDER:
                validNextTask = [TaskCode.IQTREE, TaskCode.MRBAYES];
                break;
            // 序列拼接下一步可以是对齐格式转换/多序列对齐/对齐序列优化/模型搜索/构建进化树
            case TaskCode.SEQUENCE_CONCATENATOR:
                validNextTask = [TaskCode.ALTER, TaskCode.MACSE, TaskCode.MAFFT, TaskCode.TRIMAL, TaskCode.MODEL_FINDER, TaskCode.IQTREE, TaskCode.MRBAYES];
                break;
            // 构建进化树下一步只能是合并进化树
            case TaskCode.IQTREE, TaskCode.MRBAYES:
                validNextTask = [TaskCode.ASTRAL];
                break;
            // 合并进化树只能作为最后一步
            default:
                break;
        }
        return validNextTask.includes(taskFlow[0]) && this.isTaskFlowValid(taskFlow);
    }
}
