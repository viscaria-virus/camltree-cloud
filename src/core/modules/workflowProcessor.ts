import { WorkflowProcessor } from "../../ports/core";
import { StepProgress, StepStatus, TaskResult, TaskStatus, TaskStatusChangeEvent, Workflow, WorkflowProgress, WorkflowStatus, WorkflowStatusChangeEvent } from "../../ports/models";
import { CmdToolFactory } from "../../adapters/driven/cmdTools/cmdToolFactory";
import { CmdTool } from "../../ports/driven";
import { TaskCode } from "../../common/constants";
import path from "path";
import { ParsedPath } from "path";
import { TimeStampIDGenerator } from "../../infra/uniqueIDGenerator";
import fs from "fs";
import { EventMessageQueue } from "../../infra/eventMessageQueue";
import EventEmitter from "events";

// 系统发育分析工作流处理器类
export class PhylogenyAnalysisWorkflowProcessor implements WorkflowProcessor {
    // 工作流ID
    private id: string;
    // 工作流状态
    private status: WorkflowStatus;
    // 工作流进度
    private progress: WorkflowProgress;
    // 状态变化事件监听器
    private monitor: EventEmitter;
    // 聚合任务列表
    private static readonly mergeTasks: TaskCode[] = [TaskCode.SEQUENCE_CONCATENATOR, TaskCode.ASTRAL];
    // 错误信息
    private errMsg: string;
    // 错误日志文件路径
    private errLogPath: string;
    // 工作目录
    public workDir: string;

    constructor(workDir: string) {
        // 生成工作流ID
        this.id = TimeStampIDGenerator.instance.nextID();
        // 初始化工作目录
        this.workDir = path.join(workDir, this.id);
        if (!fs.existsSync(this.workDir)) {
            fs.mkdirSync(this.workDir, { recursive: true });
        }
        // 初始化工作流错误信息和错误日志文件路径
        this.errMsg = '';
        this.errLogPath = '';
        // 初始化状态变化事件监听器
        this.monitor = new EventMessageQueue();
        // 初始化工作流状态
        this.status = WorkflowStatus.PENDING;
        // 初始化工作流进度
        this.progress = {
            progress: [],
            status: this.status
        }
    }
    // 获取工作流ID
    get workflowID(): string {
        return this.id;
    }
    // 获取工作流进度
    get workflowProgress(): WorkflowProgress {
        return this.progress;
    }
    // 获取工作流错误日志文件路径
    get logPath(): string {
        return this.errLogPath;
    }
    // 获取指定任务所属步骤进度
    private getStepProgress(taskCode: TaskCode): StepProgress {
        return this.progress.progress.find((step: StepProgress) => step.code === BigInt(taskCode)) as StepProgress;
    }
    // 监控任务状态变化
    private monitorTask(monitor: EventEmitter, taskCode: TaskCode): void {
        // 处理任务被拒绝执行事件
        monitor.on(TaskStatusChangeEvent.REFUSE, (taskResult: TaskResult) => {
            // 搜索得到任务所属步骤
            const step: StepProgress = this.getStepProgress(taskCode);
            // 更新步骤状态
            step.status = StepStatus.FAILURE;
            // 更新工作流状态
            this.status = WorkflowStatus.FAILURE;
            // 输出错误信息到工作流状态
            this.errMsg = `${taskResult.name} Error: ${taskResult.message}`;
            // 输出错误日志文件路径到工作流状态
            this.errLogPath = taskResult.log;
            // 发布工作流进度更新事件
            this.monitor.emit(WorkflowStatusChangeEvent.UPDATE);
        });
        // 处理任务开始执行事件
        monitor.on(TaskStatusChangeEvent.BEGIN, (_: string) => {
            // 搜索得到任务所属步骤
            const step: StepProgress = this.getStepProgress(taskCode);
            // 更新步骤状态
            step.status = StepStatus.RUNNING;
            // 发布工作流进度更新事件
            this.monitor.emit(WorkflowStatusChangeEvent.UPDATE);
        });
        // 处理任务完成执行事件
        monitor.on(TaskStatusChangeEvent.FINISH, (taskResult: TaskResult) => {
            // 搜索得到任务所属步骤
            const step: StepProgress = this.getStepProgress(taskCode);
            // 检查任务执行结果
            if (taskResult.success) {
                // 更新步骤进度与状态
                step.finish++;
                if (step.finish === step.total) {
                    step.status = StepStatus.SUCCESS;
                }
                // 发布工作流进度更新事件
                this.monitor.emit(WorkflowStatusChangeEvent.UPDATE);
                // 检查是否所有步骤都已完成
                if (this.progress.progress.every((step: StepProgress) => step.status === StepStatus.SUCCESS)) {
                    // 更新工作流状态
                    this.status = WorkflowStatus.SUCCESS;
                    // 发布工作流执行结束事件
                    this.monitor.emit(WorkflowStatusChangeEvent.FINISH);
                }
            } else {
                // 更新步骤状态
                step.status = StepStatus.FAILURE;
                // 更新工作流状态
                this.status = WorkflowStatus.FAILURE;
                // 发布工作流进度更新事件
                this.monitor.emit(WorkflowStatusChangeEvent.UPDATE);
                // 生成并输出错误信息到工作流状态
                this.errMsg = `${taskResult.name} Error: check log at ${taskResult.log}`;
                // 输出错误日志文件路径到工作流状态
                this.errLogPath = taskResult.log;
                // 发布工作流执行结束事件
                this.monitor.emit(WorkflowStatusChangeEvent.FINISH);
            }
        });
    }
    // 执行任务
    private executeTask(inputFile: string, outputPath: string, parameters: string[], taskCode: TaskCode): [EventEmitter, CmdTool] {
        // 创建任务执行器
        const monitor: EventEmitter = new EventMessageQueue();
        // 挂载任务状态变化事件监听回调
        this.monitorTask(monitor, taskCode);
        // 获取任务执行器
        const worker: CmdTool = CmdToolFactory.getCmdTool(taskCode, monitor) as CmdTool;
        // 异步执行任务
        worker.executeTask(inputFile, outputPath, parameters);
        return [monitor, worker];
    }
    // 生成输出文件名称
    private generateOutputFileName(inputFilePath: string, taskCode: TaskCode, format: '.nex' | '.fa' = '.fa'): [string, string] {
        // 解析输入文件路径
        const pathParts: ParsedPath = path.parse(inputFilePath);
        const inputFileName: string = pathParts.name;
        // 期望的输出文件名称
        let expectedOutputFileName: string = '';
        // 实际的输出文件名称
        let actualOutputFileName: string = '';
        switch (taskCode) {
            // ALTER输出文件名称为{inputFileName}_alt.fa或{inputFileName}_alt.nex
            case TaskCode.ALTER:
                actualOutputFileName = expectedOutputFileName = inputFileName + '_alt' + format;
                break;
            // MAFFT输出文件名称格式为{inputFileName}_msa.fa
            case TaskCode.MAFFT:
                actualOutputFileName = expectedOutputFileName = inputFileName + '_msa.fa';
                break;
            // MACSE输出文件名称格式为{inputFileName}_msa_NT.fa
            case TaskCode.MACSE:
                expectedOutputFileName = inputFileName + '_msa.fa';
                actualOutputFileName = inputFileName + '_msa_NT.fa';
                break;
            // TRIMAL输出文件名称格式为{inputFileName}_opt.fa
            case TaskCode.TRIMAL:
                actualOutputFileName = expectedOutputFileName = inputFileName + '_opt.fa';
                break;
            // MODEL_FINDER输出文件名称格式为{inputFileName}_model.log
            case TaskCode.MODEL_FINDER:
                expectedOutputFileName = inputFileName + '_model';
                actualOutputFileName = expectedOutputFileName + '.log';
                break;
            // SEQUENCE_CONCATENATOR输出文件名称为concatenated_sequences.fa
            case TaskCode.SEQUENCE_CONCATENATOR:
                actualOutputFileName = expectedOutputFileName = 'concatenated_sequences.fa';
                break;
            // IQTREE输出文件名称格式为{inputFileName}_tree.treefile
            case TaskCode.IQTREE:
                expectedOutputFileName = inputFileName + '_tree';
                actualOutputFileName = expectedOutputFileName + '.treefile';
                break;
            // MRBAYES输出文件名称格式为{inputFileName}_tree.con.tre
            case TaskCode.MRBAYES:
                actualOutputFileName = expectedOutputFileName = path.basename(inputFilePath) + '.con.tre';
                break;
            // ASTRAL输出文件名称为merged_tree.nwk
            case TaskCode.ASTRAL:
                actualOutputFileName = expectedOutputFileName = 'merged_tree.nwk';
                break;
            default:
                break;
        }
        return [
            path.join(this.workDir, expectedOutputFileName),
            path.join(this.workDir, actualOutputFileName)
        ];
    }
    // 初始化工作流进度
    private initWorkflowProgress(workflow: Workflow): void {
        for (const task of workflow.tasks) {
            // 根据任务类型生成子任务个数
            const count: bigint = PhylogenyAnalysisWorkflowProcessor.mergeTasks.includes(task.code) ? BigInt(1) : BigInt(workflow.entries.length);
            // 初始化工作流步骤进度
            const stepProgress: StepProgress = {
                code: BigInt(task.code),
                finish: BigInt(0),
                total: count,
                status: StepStatus.WAITING
            }
            this.progress.progress.push(stepProgress);
        }
    }
    // 执行工作流
    public async execute(workflow: Workflow): Promise<Error | null> {
        // 初始化任务进度
        this.initWorkflowProgress(workflow);
        // 获取输入文件路径列表
        const inputFiles: string[] = workflow.entries;
        // 初始化子任务状态列表
        const subTaskFinish: boolean[] = new Array(inputFiles.length).fill(true);
        // 遍历任务列表进行处理
        for (const [order, task] of Object.entries(workflow.tasks)) {
            // 当工作流状态为失败时返回错误
            if (this.status === WorkflowStatus.FAILURE) {
                return new Error(this.errMsg);
            }
            // 获取任务索引
            const taskIndex: number = Number(order);
            // 当任务为合并树/序列时进行特殊处理
            if ([TaskCode.SEQUENCE_CONCATENATOR, TaskCode.ASTRAL].includes(task.code)) {
                // 当上一步未完成时一直等待
                while ([StepStatus.RUNNING, StepStatus.WAITING].includes(this.progress.progress[taskIndex - 1].status)) { }
                // 当上一步失败时返回错误
                if (this.progress.progress[taskIndex - 1].status === StepStatus.FAILURE) {
                    return new Error(this.errMsg);
                }
                // 当上一步成功时继续执行后续操作
                const [expectedOutput, actualOutput]: [string, string] = this.generateOutputFileName('', task.code);
                const worker: CmdTool = this.executeTask(inputFiles.join(' '), expectedOutput, [], task.code)[1];
                // 完成任务前一直阻塞
                while ([TaskStatus.READY, TaskStatus.RUNNING].includes(worker.taskStatus)) { }
                // 生成下一步输入
                inputFiles.splice(0, inputFiles.length, actualOutput);
                continue;
            }
            // 遍历输入文件列表以执行任务
            for (const [order, inputFile] of Object.entries(inputFiles)) {
                // 获取输入文件索引
                const inputIndex: number = Number(order);
                // 生成输出文件路径
                let expectedOutput: string;
                let actualOutput: string;
                if (task.code === TaskCode.MRBAYES) {
                    // 对于MrBayes任务，先生成ALTER输出路径
                    var [expectedAlterOutput, actualAlterOutput]: [string, string] = this.generateOutputFileName(inputFile, TaskCode.ALTER, '.nex');
                    // ALTER任务输出路径是MrBayes任务的输入路径
                    [expectedOutput, actualOutput] = this.generateOutputFileName(actualAlterOutput, TaskCode.MRBAYES);
                } else {
                    [expectedOutput, actualOutput] = this.generateOutputFileName(inputFile, task.code);
                }
                // 异步执行子任务处理操作
                setTimeout(() => {
                    // 当上一步子任务未完成时一直阻塞
                    while (!subTaskFinish[inputIndex]) { }
                    // 当工作流状态为失败时直接放弃全部操作
                    if (this.status === WorkflowStatus.FAILURE) {
                        return;
                    }
                    // 开始当前任务前重置子任务完成状态
                    subTaskFinish[inputIndex] = false;
                    // 当任务为MrBayes时进行特殊处理
                    if (task.code === TaskCode.MRBAYES) {
                        let [alterFinish, modelFounded]: [boolean, boolean] = [false, false];
                        // 执行多序列对齐格式转换
                        const [alterMonitor, alterWorker]: [EventEmitter, CmdTool] = this.executeTask(inputFile, expectedAlterOutput, [], TaskCode.ALTER);
                        alterMonitor.on(TaskStatusChangeEvent.FINISH, (_: TaskResult) => {
                            alterFinish = true;
                        });
                        // 执行核苷酸/氨基酸取代模型搜索
                        const [expectedModelOutput, actualModelOutput]: [string, string] = this.generateOutputFileName(inputFile, TaskCode.MODEL_FINDER);
                        const [modelMonitor, modelWorker]: [EventEmitter, CmdTool] = this.executeTask(inputFile, expectedModelOutput, [], TaskCode.MODEL_FINDER);
                        modelMonitor.on(TaskStatusChangeEvent.FINISH, (_: TaskResult) => {
                            modelFounded = true;
                        });
                        // 等待两步任务完成
                        while (!alterFinish || !modelFounded) { }
                        // 任意一步任务失败时直接中止操作
                        if (alterWorker.taskStatus === TaskStatus.FAILURE || modelWorker.taskStatus === TaskStatus.FAILURE) {
                            return;
                        }
                        // 处理MrBayes的输入文件
                        const records: string[] = fs.readFileSync(actualModelOutput).toString().split('\n');
                        for (const line of records) {
                            if (line.startsWith('Bayesian Information Criterion:')) {
                                task.arguments = line.split(':')[1].trim().split('+').join(' ');
                                break;
                            }
                        }
                    }
                    // 当任务为MrBayes时，输入文件路径为ALTER输出文件路径
                    const entry: string = task.code === TaskCode.MRBAYES ? actualAlterOutput : inputFile;
                    // 执行任务
                    const monitor: EventEmitter = this.executeTask(entry, expectedOutput, task.arguments.split(' '), task.code)[0];
                    // 监听任务完成事件
                    monitor.on(TaskStatusChangeEvent.FINISH, (taskResult: TaskResult) => {
                        // 根据任务执行状态更新对应子任务完成状态
                        if (taskResult.success) {
                            subTaskFinish[inputIndex] = true;
                        }
                    });
                }, 0);
                // 生成下一步输入
                inputFiles[inputIndex] = actualOutput;
            }
        }
        // 等待工作流执行结果确定后返回
        while (this.status === WorkflowStatus.PENDING) { }
        return (this.status === WorkflowStatus.FAILURE) ? new Error(this.errMsg) : null;
    }
    // 注册状态变化事件监听器
    public handleStatusChange(event: WorkflowStatusChangeEvent, callback: (...args: any[]) => void): void {
        this.monitor.on(event, callback);
    }
}
