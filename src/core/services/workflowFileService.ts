import { LocalFileStorage } from "../../adapters/driven/storage/localFileStorage";
import { FileService } from "../../ports/core";
import { FileStorage } from "../../ports/driven";
import config from "../../common/config";
import AdmZip from "adm-zip";
import { ReadStream } from "fs";

// 工作流文件服务
export class WorkflowFileService implements FileService {
    // 类属性挂载文件存储系统局部单例
    private static storage: FileStorage = new LocalFileStorage(config.workdir);

    // 上传文件
    public uploadFile(fileName: string, content: ReadStream): Error | null {
        return WorkflowFileService.storage.save(fileName, content);
    }
    // 生成压缩文件
    private generateZipFile(fileID: string): string {
        // 获取待压缩目录路径
        const dirPath: string = WorkflowFileService.storage.getRealPath(fileID);
        // 生成压缩文件
        const zipPath: string = dirPath + ".zip";
        // 压缩目录
        const zip = new AdmZip();
        zip.addLocalFile(dirPath);
        zip.writeZip(zipPath);
        return zipPath;
    }
    // 同步下载文件
    public downloadFile(fileID: string): ReadStream {
        // 生成压缩文件
        const zipPath: string = this.generateZipFile(fileID);
        // 调用文件存储系统读取文件
        return WorkflowFileService.storage.load(zipPath);
    }
    // 解析文件路径
    public parseFilePath(filePath: string): string {
        return WorkflowFileService.storage.getRealPath(filePath);
    }
}
