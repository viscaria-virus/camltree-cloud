import { FileService, WorkflowProcessor, WorkflowService, WorkflowValidator } from "../../ports/core";
import { PhylogenyAnalysisWorkflowProcessor } from "../modules/workflowProcessor";
import config from "../../common/config";
import { GlobalStore } from "../../infra/stateManager";
import { Workflow, WorkflowProgress } from "../../ports/models";
import { PhylogenyAnalysisWorkflowValidator } from "../modules/workflowValidator";
import { StateManager } from "../../ports/infra";
import { WorkflowFileService } from "./workflowFileService";
import { FileStorage } from "../../ports/driven";

// 工作流服务
export class PhylogenyWorkflowService implements WorkflowService {
    // 工作流校验器
    private validator: WorkflowValidator;
    // 工作流集合
    private workflows: StateManager;
    // 工作流文件服务
    private fileService: FileService;

    constructor() {
        // 挂载工作流校验器
        this.validator = PhylogenyAnalysisWorkflowValidator.instance;
        // 挂载工作流集合
        this.workflows = GlobalStore.instance;
        // 挂载文件存储系统
        this.fileService = new WorkflowFileService();
    }
    // 校验工作流
    public verifyWorkflow(workflow: Workflow): Error | null {
        return this.validator.validate(workflow);
    }
    // 获取工作流处理器
    public getProcessor(): WorkflowProcessor {
        // 创建工作流处理器实例
        const processor: WorkflowProcessor = new PhylogenyAnalysisWorkflowProcessor(config.workdir);
        // 注册工作流处理器
        const workflowID: string = processor.workflowID;
        this.workflows.setState(workflowID, processor);
        return processor;
    }
    // 执行工作流
    public executeWorkflow(processor: WorkflowProcessor, workflow: Workflow): string {
        // 解析工作流中输入文件路径
        for (const [index, entry] of Object.entries(workflow.entries)) {
            workflow.entries[Number(index)] = this.fileService.parseFilePath(entry)
        }
        processor.execute(workflow);
        return processor.workflowID;
    }
    // 获取指定工作流处理器
    public getWorkflowProcessor(workflowID: string): WorkflowProcessor | null {
        const processor = this.workflows.getState(workflowID);
        if (!processor) {
            return null;
        }
        return processor as WorkflowProcessor;
    }
    // 获取指定工作流进度
    public getWorkflowProgress(workflowID: string): WorkflowProgress | null {
        const workflow = this.getWorkflowProcessor(workflowID);
        if (!workflow) {
            return null;
        }
        return workflow.workflowProgress;
    }
}
