import path from "path";
import { FileStorage } from "../../../ports/driven";
import fs, { ReadStream } from "fs";

export class LocalFileStorage implements FileStorage {
    // 文件存储根目录
    private root: string;

    constructor(rootDir: string) {
        // 从配置中加载文件存储根目录
        this.root = rootDir;
    }
    // 将文件路径解析为本机文件系统的真实路径
    public getRealPath(filePath: string): string {
        if ((!filePath.startsWith('./')) && (!filePath.startsWith('/'))) {
            filePath = './' + filePath;
        }
        return path.resolve(this.root, filePath);
    }
    // 保存文件
    public save(filePath: string, content: ReadStream): Error | null {
        // 初始化写入完成状态与错误
        let finish: boolean = false;
        let err: Error | null = null;
        // 采用流式读写文件防止内存溢出
        const savedPath = this.getRealPath(filePath);
        const writer = fs.createWriteStream(savedPath);
        // 监听读取流错误事件
        content.on('error', (error: Error) => {
            err = error;
            writer.end();
        })
        // 监听写入流完成事件
        writer.on('finish', () => {
            finish = true;
        })
        // 监听写入流错误事件
        writer.on('error', (error: Error) => {
            err = error;
            finish = true;
        })
        // 连接读写流执行写入
        content.pipe(writer);
        // 等待写入完成
        while (!finish) { }
        return err;
    }
    // 读取文件
    public load(filePath: string): ReadStream {
        return fs.createReadStream(this.getRealPath(filePath));
    }
    // 删除文件
    public delete(filePath: string): Error | null {
        try {
            fs.rmSync(this.getRealPath(filePath));
        } catch (error) {
            return error as Error;
        }
        return null;
    }
}
