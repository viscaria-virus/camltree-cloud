import path from "path";
import { CmdToolTemplate } from "./cmdToolTemplate";
import { phylotreeSuffix } from "../../../common/constants";
import fs from "fs";

// 进化树合并工具模板类
abstract class TreeMergingTool extends CmdToolTemplate {
    // 实现校验输入文件的抽象方法
    protected override verifyInput(filePath: string): boolean {
        // 根据文件路径中的文件扩展名判断是否为有效的输入文件
        const suffix: string = path.extname(filePath);
        const isInputFileValid: boolean = phylotreeSuffix.includes(suffix);
        // 如果不是有效的输入文件，则发送错误消息并放弃任务
        if (!isInputFileValid) {
            const validSuffix: string = phylotreeSuffix.join('/');
            const errMsg: string = `Invalid file suffix '${suffix}', it must be ${validSuffix}`;
            this.abortTask(errMsg);
        }
        return isInputFileValid;
    }
}

// ASTRAL进化树合并工具适配器模块
export class ASTRAL extends TreeMergingTool {
    // 准备ASTRAL命令
    protected override prepareCommand(_: string): null {
        // 生成并设置日志文件路径
        const outputDir: string = path.dirname(this.outputFilePath);
        this._logFilePath = path.join(outputDir, 'log.txt');
        // 生成输入文件
        const inputTreeFile: string = this.generateInputFile();
        // 生成并设置命令
        this._command = `${this.executableFilePath} -i ${inputTreeFile} -o ${this.outputFilePath} 2> ${this._logFilePath}`;
        return null;
    }
    // 生成输入文件
    private generateInputFile(): string {
        // 生成输入的树文件路径
        const outputDir: string = path.dirname(this.outputFilePath);
        const inputTreeFile: string = path.join(outputDir, 'input.nwk');
        // 创建空文件
        fs.writeFileSync(inputTreeFile, '');
        // 将输入的所有树文件内容合并到上述空文件中
        const inputTrees: string[] = this.inputFilePath.split(' ');
        for (const treeFile of inputTrees) {
            fs.appendFileSync(inputTreeFile, fs.readFileSync(treeFile));
        }
        return inputTreeFile;
    }
}
