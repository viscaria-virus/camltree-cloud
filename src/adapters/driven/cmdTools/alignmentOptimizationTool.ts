import path from "path";
import { AlignmentProcessingTool } from "./alignmentProcessingTool";

// TrimAl对齐优化工具适配器模块
export class TrimAl extends AlignmentProcessingTool {
    // 准备trimAl命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath.replace(outputFileExtName, '.log');
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${parameters} -in ${this.inputFilePath} -out ${this.outputFilePath} 2>${this._logFilePath}`;
        return null;
    }
}
