import { AlignmentProcessingTool } from "./alignmentProcessingTool";

// ModelFinder核苷酸/氨基酸替代模型搜索工具适配器模块
export class ModelFinder extends AlignmentProcessingTool {
    // 准备ModelFinder命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        this._logFilePath = this.outputFilePath + '.log';
        // 生成并设置命令
        this._command = `${this.executableFilePath} -s ${this.inputFilePath} --prefix ${this.outputFilePath} -m TEST --mset ${parameters}`;
        return null;
    }
}
