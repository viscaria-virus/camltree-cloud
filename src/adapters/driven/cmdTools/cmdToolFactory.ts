import { TaskCode } from "../../../common/constants"
import { CmdTool } from "../../../ports/driven"
import { ALTER } from "./alignmentConvertionTool"
import config from "../../../common/config"
import EventEmitter from "events"
import { MACSE, MAFFT } from "./sequenceAlignmentTool"
import { TrimAl } from "./alignmentOptimizationTool"
import { ModelFinder } from "./modelSearchTool"
import { SequenceConcatenator } from "./sequenceConcatenationTool"
import { IQTREE, MrBayes } from "./treeConstructionTool"
import { ASTRAL } from "./treeMergingTool"

// 命令行工具工厂类
export abstract class CmdToolFactory {
    public static getCmdTool(taskCode: TaskCode, taskMonitor: EventEmitter): CmdTool | null {
        switch (taskCode) {
            case TaskCode.ALTER:
                return new ALTER(config.alter, taskMonitor);
            case TaskCode.MAFFT:
                return new MAFFT(config.mafft, taskMonitor);
            case TaskCode.MACSE:
                return new MACSE(config.macse, taskMonitor);
            case TaskCode.TRIMAL:
                return new TrimAl(config.trimal, taskMonitor);
            case TaskCode.MODEL_FINDER:
                // 使用IQ-TREE内置的ModelFinder
                return new ModelFinder(config.iqtree, taskMonitor);
            case TaskCode.SEQUENCE_CONCATENATOR:
                return new SequenceConcatenator(config.concatenator, taskMonitor);
            case TaskCode.IQTREE:
                return new IQTREE(config.iqtree, taskMonitor);
            case TaskCode.MRBAYES:
                return new MrBayes(config.mrbayes, taskMonitor);
            case TaskCode.ASTRAL:
                return new ASTRAL(config.astral, taskMonitor);
            default:
                return null;
        }
    }
}
