import path from "path";
import { CmdToolTemplate } from "./cmdToolTemplate";
import { sequenceSuffix } from "../../../common/constants";

// 序列连接工具适配器模块
export class SequenceConcatenator extends CmdToolTemplate {
    // 实现校验输入文件的抽象方法
    protected override verifyInput(filePaths: string): boolean {
        const filePathList: string[] = filePaths.trim().split(' ');
        let isInputFileValid: boolean = true;
        for (const filePath of filePathList) {
            // 根据文件路径中的文件扩展名判断是否为有效的输入文件
            const suffix: string = path.extname(filePath);
            isInputFileValid = sequenceSuffix.includes(suffix);
            // 如果不是有效的输入文件，则发送错误消息并放弃任务
            if (!isInputFileValid) {
                const validSuffix: string = sequenceSuffix.join('/');
                const errMsg: string = `Invalid file suffix '${suffix}', it must be ${validSuffix}`;
                this.abortTask(errMsg);
                break;
            }
        }
        return isInputFileValid;
    }
    // 准备序列连接命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath.replace(outputFileExtName, '.log');
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${parameters} -i ${this.inputFilePath} -o ${this.outputFilePath} 2>${this._logFilePath}`;
        return null;
    }
}
