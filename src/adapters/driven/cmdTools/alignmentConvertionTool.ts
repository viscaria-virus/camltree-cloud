import path from "path";
import { AlignmentProcessingTool } from "./alignmentProcessingTool";

// ALTER对齐格式转换工具适配器模块
export class ALTER extends AlignmentProcessingTool {
    // 允许的输出格式
    private static readonly outputFormats: string[] = ['FASTA', 'NEXUS'];
    // 允许的输出程序
    private static readonly outputPrograms: string[] = ['MrBayes', 'GENERAL'];
    // 准备ALTER命令
    protected override prepareCommand(parameters: string): Error | null {
        // 解析参数并进行校验
        const [outputFormat, outputProgram] = parameters.split(' ');
        // 当输出格式不在允许范围内时，返回错误
        if (!ALTER.outputFormats.includes(outputFormat)) {
            const errMsg: string = `Invalid output format '${outputFormat}', it must be ${ALTER.outputFormats.join('/')}`;
            return new Error(errMsg);
        }
        // 当输出程序不在允许范围内时，返回错误
        if (!ALTER.outputPrograms.includes(outputProgram)) {
            const errMsg: string = `Invalid output program '${outputProgram}', it must be ${ALTER.outputPrograms.join('/')}`;
            return new Error(errMsg);
        }
        // 获取程序运行平台
        let platform: string;
        switch (process.platform) {
            case 'linux':
                platform = 'Linux';
                break;
            case 'win32':
                platform = 'Windows';
                break;
            case 'darwin':
                platform = 'MacOS';
                break;
            // 当运行平台不是linux/win32/darwin时，返回未支持平台错误
            default:
                const errMsg: string = `Unsupported platform ${process.platform},it must be linux/win32/darwin`;
                return new Error(errMsg);
        }
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath.replace(outputFileExtName, '_convert.log');
        // 生成并设置命令
        this._command = `java -jar ${this.executableFilePath} -i ${this.inputFilePath} -ia -o ${this.outputFilePath} -of ${outputFormat} -op ${outputProgram} -oo ${platform} 2>${this._logFilePath}`;
        return null;
    }
}