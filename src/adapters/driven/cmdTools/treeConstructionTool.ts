import path from 'path';
import fs from 'fs';
import { AlignmentProcessingTool } from './alignmentProcessingTool';

// IQTREE进化树构建工具适配器模块
export class IQTREE extends AlignmentProcessingTool {
    // 准备IQ-TREE命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath + '.log';
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${parameters} -s ${this.inputFilePath} --prefix ${this.outputFilePath.replace(outputFileExtName, '')}`;
        return null;
    }
}

// MrBayes进化树构建工具适配器模块
export class MrBayes extends AlignmentProcessingTool {
    // 准备MrBayes命令
    protected override prepareCommand(parameters: string): null {
        // 解析模型参数
        const modelParams: string[] = parameters.split(' ');
        // 根据选定模型生成模型参数
        let [nst, freq, rates, Ngammacat]: [number, string, string, number] = [0, '', '', 0];
        // 生成nst参数
        const modelBase = modelParams.shift() as string;
        if (['JC', 'JC69', 'F81'].includes(modelBase)) {
            nst = 1;
        } else if (['HKY', 'HKY85', 'K2P', 'K80'].includes(modelBase)) {
            nst = 2;
        } else if (!['WAG', 'JTT', 'VT', 'Blosum62', 'Dayhoff', 'mtREV', 'mtMAM', 'rtREV', 'cpREV'].includes(modelBase)) {
            nst = 6;
        }
        // 生成freq参数
        if (['SYM', 'K2P', 'K80', 'JC', 'JC69'].includes(modelBase)) {
            freq = 'equal';
        } else if (modelParams[0].startsWith('F')) {
            if (modelParams[0] === 'F') {
                freq = 'empirical';
            } else if (modelParams[0] === 'FQ') {
                freq = 'equal';
            }
            modelParams.shift();
        }
        // 生成rates参数
        const restArgv: string = modelParams.join('+');
        if (restArgv === 'I') {
            rates = 'propinv';
        } else if (restArgv.startsWith('G')) {
            rates = 'gamma';
            Ngammacat = Number(restArgv.slice(1));
        } else if (restArgv.startsWith('I+G')) {
            rates = 'invgamma';
            Ngammacat = Number(restArgv.slice(3));
        }
        const modelSettingFields: string[] = ['lset'];
        // 当nst不为0时说明为核苷酸取代模型，插入nst参数片段
        if (nst !== 0) {
            modelSettingFields.push(`nst=${nst}`);
        }
        // 当rates不为空时插入rates参数片段
        if (rates !== '') {
            modelSettingFields.push(`rates=${rates}`);
        }
        // 当Ngammacat不为0时插入Ngammacat参数片段
        if (Ngammacat !== 0) {
            modelSettingFields.push(`Ngammacat=${Ngammacat}`);
        }
        const modelSettingLines: string[] = [modelSettingFields.join(' ') + ';'];
        // 当lset参数行为空时舍弃该行
        if (modelSettingLines[0] === 'lset;') {
            modelSettingLines.pop();
        }
        // 当nst为0时说明为氨基酸取代模型，插入相应片段
        if (nst === 0) {
            const Aamodelpr: string = (modelBase === 'JTT') ? 'jones' : modelBase.toLowerCase();
            modelSettingLines.push(`prset Aamodelpr=fixed(${Aamodelpr});`);
        }
        // 当freq不为空时插入freq参数行
        if (freq !== '') {
            modelSettingLines.push(`prset statefreqpr = fixed(${freq});`);
        }
        // 拼接根据模型生成的运行参数
        const modelSettings: string = modelSettingLines.join('\n');
        // 生成并设置日志文件路径
        const outputDir: string = path.dirname(this.outputFilePath);
        this._logFilePath = path.join(outputDir, 'log.txt');
        // 生成运行参数
        const runningParams: string[] = [
            'begin mrbayes;',
            `log start filename = ${this._logFilePath};`,
            modelSettings,
            'mcmcp ngen=2000000 samplefreq=1000 nchains=4 nruns=2 savebrlens=yes checkpoint=yes checkfreq=1000;',
            'mcmc;',
            'sumt conformat=Figtree contype=Allcompat relburnin=yes burninfrac=0.25;',
            'sump relburnin=yes burninfrac=0.25;',
            'end;'
        ];
        // 将输入文件复制到输出目录
        const inputFile: string = path.join(outputDir, path.basename(this.inputFilePath));
        if (path.resolve(inputFile) !== path.resolve(this.inputFilePath)) {
            fs.copyFileSync(this.inputFilePath, inputFile);
        }
        // 将运行参数追加到输入文件
        fs.appendFileSync(inputFile, runningParams.join('\n'));
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${inputFile}`;
        return null;
    }
}
