import path from "path";
import { CmdToolTemplate } from "./cmdToolTemplate";
import { sequenceSuffix } from "../../../common/constants";

// 序列对齐工具模板类
abstract class SequenceAlignmentTool extends CmdToolTemplate {
    // 实现校验输入文件的抽象方法
    protected override verifyInput(filePath: string): boolean {
        // 根据文件路径中的文件扩展名判断是否为有效的输入文件
        const suffix: string = path.extname(filePath);
        const isInputFileValid: boolean = sequenceSuffix.includes(suffix)
        // 如果不是有效的输入文件，则发送错误消息并放弃任务
        if (!isInputFileValid) {
            const validSuffix: string = sequenceSuffix.join('/');
            const errMsg: string = `Invalid file suffix '${suffix}', it must be ${validSuffix}`;
            this.abortTask(errMsg);
        }
        return isInputFileValid;
    }
}

// MAFFT序列对齐工具适配器模块
export class MAFFT extends SequenceAlignmentTool {
    // 准备MAFFT命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath.replace(outputFileExtName, '.log');
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${parameters} ${this.inputFilePath}>${this.outputFilePath} 2>${this._logFilePath}`;
        return null;
    }
}

// MACSE序列对齐工具适配器模块
export class MACSE extends SequenceAlignmentTool {
    // 准备MACSE命令
    protected override prepareCommand(parameters: string): null {
        // 生成并设置日志文件路径
        const outputFileExtName: string = path.extname(this.outputFilePath);
        this._logFilePath = this.outputFilePath.replace(outputFileExtName, '.log');
        // 生成并设置命令
        this._command = `${this.executableFilePath} ${parameters} -seq ${this.inputFilePath} -out_NT ${this.outputFilePath.replace(outputFileExtName, '_NT.fa')} -out_AA ${this.outputFilePath.replace(outputFileExtName, '_AA.fa')} > ${this._logFilePath}`;
        return null;
    }
}
