import fs from "fs";
import { CmdTool } from "../../../ports/driven";
import path from "path";
import child_process from "child_process";
import { TaskResult, TaskStatus, TaskStatusChangeEvent } from "../../../ports/models";
import EventEmitter from "events";

// 命令行工具模板抽象类，实现了命令行工具接口
export abstract class CmdToolTemplate implements CmdTool {
    // 输入文件路径
    private _inputFilePath!: string;
    // 执行命令
    protected _command!: string;
    // 任务状态
    private _taskStatus!: TaskStatus;
    // 日志文件路径
    protected _logFilePath!: string;
    // 命令行工具可执行文件路径
    protected executableFilePath!: string;
    // 任务名称
    private taskName!: string;
    // 任务监控器
    private taskMonitor: EventEmitter;
    // 输出文件路径
    protected outputFilePath!: string;
    // 构造器方法
    constructor(filePath: string, monitor: EventEmitter) {
        // 挂载任务监视器
        this.taskMonitor = monitor;
        // 校验可执行文件路径
        if (!this.verifyPath(filePath)) {
            return;
        }
        // 设置可执行文件路径
        this.executableFilePath = filePath;
        // 任务初始化
        this.init();
    }
    // 设置输入文件路径
    protected set inputFilePath(filePath: string) {
        // 校验输入文件路径
        if ((!this.verifyPath(filePath)) || (!this.verifyInput(filePath))) {
            return;
        }
        this._inputFilePath = filePath;
    }
    // 获取输入文件路径
    protected get inputFilePath(): string {
        return this._inputFilePath;
    }
    // 获取命令
    get command(): string {
        return this._command;
    }
    // 获取任务状态
    get taskStatus(): TaskStatus {
        return this._taskStatus;
    }
    // 获取日志文件路径
    get logFilePath(): string {
        return this._logFilePath;
    }
    // 任务初始化
    private init(): void {
        // 将未设置的属性置空
        this._inputFilePath = '';
        this.outputFilePath = '';
        this._logFilePath = '';
        this._command = '';
        // 初始化任务状态
        this._taskStatus = TaskStatus.READY;
    }
    // 放弃任务
    protected abortTask(errMsg: string): void {
        // 更新任务状态
        this._taskStatus = TaskStatus.FAILURE;
        // 生成任务执行结果消息
        const taskResult: TaskResult = {
            name: this.taskName,
            execute: false,
            success: false,
            message: errMsg,
            log: ''
        };
        // 通知任务监视器
        this.taskMonitor.emit(TaskStatusChangeEvent.REFUSE, taskResult);
    }
    // 校验文件路径
    private verifyPath(filePath: string): boolean {
        const absFilePath: string = path.resolve(filePath);
        const isFileExist: boolean = fs.existsSync(absFilePath);
        // 文件不存在时放弃任务
        if (!isFileExist) {
            const errMsg: string = `Invalid file path ${filePath} or file does not exist!`;
            this.abortTask(errMsg);
        }
        return isFileExist;
    }
    // 结束任务
    private finishTask(code: number): void {
        // 判断任务执行是否成功
        const taskSuccess: boolean = code === 0;
        // 生成任务执行结果消息
        const taskResult: TaskResult = {
            name: this.taskName,
            execute: true,
            success: taskSuccess,
            message: '',
            log: this._logFilePath
        };
        // 更新任务状态
        this._taskStatus = (taskSuccess) ? TaskStatus.SUCCESS : TaskStatus.FAILURE;
        // 通知任务监视器
        this.taskMonitor.emit(TaskStatusChangeEvent.FINISH, taskResult);
    }
    // 执行任务
    public executeTask(inputFilePath: string, outputFilePath: string, parameters: string[]): void {
        // 解析任务参数
        const params: string = this.parseParams(inputFilePath, outputFilePath, parameters);
        // 准备任务命令
        const err: Error | null = this.prepareCommand(params);
        if (err !== null) {
            this.abortTask(err.message);
            return;
        }
        // 创建子进程执行任务
        const task = child_process.exec(this._command, () => { });
        // 更新任务状态
        this._taskStatus = TaskStatus.RUNNING;
        // 通知任务监视器
        this.taskMonitor.emit(TaskStatusChangeEvent.BEGIN, this.taskName);
        // 监听子进程退出事件，挂载结束任务回调方法
        task.on('exit', this.finishTask);
    }
    // 解析任务参数
    private parseParams(inputFilePath: string, outputFilePath: string, parameters: string[]): string {
        // 设置输入文件路径
        this.inputFilePath = inputFilePath;
        // 设置输出文件路径
        this.outputFilePath = outputFilePath;
        // 拼接任务参数字符串
        const params: string = parameters.join(' ');
        return params;
    }
    // 抽象方法，用于生成任务命令，由子类实现
    protected abstract prepareCommand(parameters: string): Error | null;
    // 抽象方法，用于校验输入文件路径，由子类实现
    protected abstract verifyInput(filePath: string): boolean;
}
