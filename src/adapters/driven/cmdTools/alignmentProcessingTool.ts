import path from "path";
import { CmdToolTemplate } from "./cmdToolTemplate";
import { alignmentSuffix } from "../../../common/constants";

// 对齐处理工具模板类
export abstract class AlignmentProcessingTool extends CmdToolTemplate{
    // 实现校验输入文件的抽象方法
    protected override verifyInput(filePath: string): boolean {
        // 根据文件路径中的文件扩展名判断是否为有效的输入文件
        const suffix: string = path.extname(filePath);
        const isInputFileValid: boolean = alignmentSuffix.includes(suffix);
        // 如果不是有效的输入文件，则发送错误消息并放弃任务
        if (!isInputFileValid) {
            const validSuffix: string = alignmentSuffix.join('/');
            const errMsg: string = `Invalid file suffix '${suffix}', it must be ${validSuffix}`;
            this.abortTask(errMsg);
        }
        return isInputFileValid;
    }
}
