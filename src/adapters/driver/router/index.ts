import Router from "koa-router";
import { WorkflowHandler } from "../apis/workflowHandler";
import { WorkflowFileHandler } from "../apis/workflowFileHandler";

// 创建workflow子路由
const workflowRouter: Router = new Router({
    prefix: '/workflow'
});
// 在workflow子路由上注册API
WorkflowHandler.instance.registerAPI(workflowRouter);
WorkflowFileHandler.instance.registerAPI(workflowRouter);

// 创建总路由
const router: Router = new Router({
    prefix: '/api/v1/camltree'
});
// 注册子路由
router.use(workflowRouter.routes());

export default router;
