import Joi from 'joi';
import Koa from 'koa';
import { ErrorResponse } from '../../../ports/models';

// 校验请求参数是否符合要求
export function isRequestValid(ctx: Koa.Context, requestParams: object, requestSchema: Joi.ObjectSchema): boolean {
    // 调用joi进行参数校验
    const result = requestSchema.validate(requestParams);
    const err = result.error;
    // 如果校验失败，设置错误响应
    if (err) {
        ctx.status = 400;
        ctx.body = {
            cause: err.name,
            detail: err.message,
            solution: ''
        } as ErrorResponse;
        return false;
    }
    return true;
}
