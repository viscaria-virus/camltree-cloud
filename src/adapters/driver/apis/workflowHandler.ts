import { PhylogenyWorkflowService } from "../../../core/services/workflowService";
import { WorkflowProcessor, WorkflowService } from "../../../ports/core";
import Router from "koa-router";
import { ErrorResponse, Workflow, WorkflowProgress, WorkflowStatusChangeEvent } from "../../../ports/models";
import Koa from 'koa';
import WebSocket, { WebSocketServer } from "ws";
import { wss } from "../../../infra/webSocketServer";
import { isRequestValid } from "../utils/requestValidator";
import { RequestSchemas } from "../../../ports/models";

// 工作流服务路由处理器
export class WorkflowHandler {
    // 类属性挂载单例
    private static readonly handler: WorkflowHandler = new WorkflowHandler();
    // WebSocket服务器
    private ws: WebSocketServer;
    // 服务模块
    private service: WorkflowService;
    // 构造函数设置为私有防止外部实例化
    private constructor() {
        // 创建并挂载服务模块
        this.service = new PhylogenyWorkflowService();
        // 挂载WebSocket服务器
        this.ws = wss;
    }
    // 类方法获取单例
    static get instance(): WorkflowHandler {
        return this.handler;
    }
    // 注册API
    public registerAPI(router: Router): void {
        router.post('/', this.Post.bind(this));
        router.get('/:id', this.Get.bind(this));
    }
    // POST /workflow
    private Post(ctx: Koa.Context) {
        // 解析请求体
        const body = ctx.request.body;
        if (!isRequestValid(ctx, body, RequestSchemas.PostWorkflow)) {
            return;
        }
        const workflow: Workflow = ctx.request.body as Workflow;
        // 校验工作流
        const err = this.service.verifyWorkflow(workflow)
        // 若校验失败，返回错误响应
        if (err) {
            ctx.status = 400;
            ctx.body = {
                cause: err.message,
                detail: '',
                solution: ''
            } as ErrorResponse;
            return;
        }
        // 执行工作流
        const processor: WorkflowProcessor = this.service.getProcessor();
        const id: string = this.service.executeWorkflow(processor, workflow);
        // 返回成功响应
        ctx.status = 202;
        ctx.body = { id };
    }
    // GET /workflow/:id
    private Get(ctx: Koa.Context) {
        // 获取工作流ID
        const id = ctx.params.id;
        // 校验工作流ID是否合法
        if (!isRequestValid(ctx, { id }, RequestSchemas.GetByWorkflowID)) {
            return;
        }
        // 获取工作流进度
        const progress = this.service.getWorkflowProgress(id);
        // 工作流进度不存在时返回错误响应
        if (!progress) {
            ctx.status = 404;
            ctx.body = {
                cause: `Workflow ${id} not found`,
                detail: '',
                solution: ''
            } as ErrorResponse;
            return;
        }
        // 检查客户端是否申请同步进度
        const sync = ctx.query['sync'];
        if (sync === 'true') {
            // 获取工作流处理器
            const processor = this.service.getWorkflowProcessor(id) as WorkflowProcessor;
            // 切换为WebSocket连接
            this.ws.handleUpgrade(ctx.req, ctx.socket, Buffer.alloc(0), (client: WebSocket) => {
                // 当工作流进度更新时推送给客户端
                processor.handleStatusChange(WorkflowStatusChangeEvent.UPDATE, () => {
                    const progress: WorkflowProgress = this.service.getWorkflowProgress(id) as WorkflowProgress;
                    const data: string = JSON.stringify(progress);
                    client.send(data);
                });
                // 当工作流执行结束时关闭WebSocket连接
                processor.handleStatusChange(WorkflowStatusChangeEvent.FINISH, () => {
                    client.close();
                });
            });
        } else {
            // 返回成功响应
            ctx.status = 200;
            ctx.body = progress as WorkflowProgress;
        }
    }
}
