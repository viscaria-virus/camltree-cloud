import Router from "koa-router";
import { WorkflowFileService } from "../../../core/services/workflowFileService";
import { FileService } from "../../../ports/core";
import { ErrorResponse } from "../../../ports/models";
import Koa from 'koa';
import { File } from 'formidable';
import fs, { ReadStream } from 'fs';
import { isRequestValid } from "../utils/requestValidator";
import { RequestSchemas } from "../../../ports/models";

// 工作流文件服务路由处理器
export class WorkflowFileHandler {
    // 类属性挂载单例
    private static readonly handler: WorkflowFileHandler = new WorkflowFileHandler();
    // 服务模块
    private service: FileService;
    // 构造函数私有化防止外部实例化
    private constructor() {
        // 创建并挂载服务模块
        this.service = new WorkflowFileService();
    }
    // 类方法获取单例
    static get instance(): WorkflowFileHandler {
        return this.handler;
    }
    // 注册API
    public registerAPI(router: Router): void {
        router.post('/upload', this.Post.bind(this));
        router.get('/download/:id', this.Get.bind(this));
    }
    // POST /workflow/upload
    private Post(ctx: Koa.Context) {
        // 从请求中获取文件
        const fileData = ctx.request.files?.file;
        // 上传文件为空时响应客户端错误
        if (fileData === undefined) {
            ctx.status = 400;
            ctx.body = {
                cause: 'empty file',
                detail: 'no file found in request',
                solution: 'please provide at least one file in the request body'
            } as ErrorResponse;
            return;
        }
        // 将文件数据解析为文件对象列表
        const files: File[] = Array.isArray(fileData) ? fileData : [fileData];
        // 调用服务上传文件
        for (const file of files) {
            const savedName: string = file.originalFilename as string;
            const fileContent: ReadStream = fs.createReadStream(file.filepath);
            const err = this.service.uploadFile(savedName, fileContent);
            // 文件上传出错时响应失败
            if (err) {
                ctx.status = 500;
                ctx.body = {
                    cause: err.message,
                    detail: '',
                    solution: ''
                } as ErrorResponse;
                return;
            }
        }
        // 全部文件上传成功时响应成功
        ctx.status = 200;
    }
    // GET /workflow/download/:id
    private Get(ctx: Koa.Context) {
        // 获取工作流ID
        const id = ctx.params.id;
        // 校验工作流ID是否合法
        if (!isRequestValid(ctx, { id }, RequestSchemas.GetByWorkflowID)) {
            return;
        }
        // 调用服务下载文件
        const file: ReadStream = this.service.downloadFile(id);
        // 生成文件名称
        const fileName: string = `${id}.zip`
        // 响应文件内容
        ctx.set('Content-Disposition', `attachment; filename=${fileName}`)
        ctx.set('Content-Type', 'application/octet-stream')
        ctx.status = 200;
        ctx.body = file;
    }
}
