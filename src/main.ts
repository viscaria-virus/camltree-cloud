import Koa from 'koa';
import router from './adapters/driver/router';
import config from './common/config';
import koaBody from 'koa-body';
import koaCompress from 'koa-compress';
import KoaLogger from 'koa-logger';
const KoaErrorHandler = require('koa-error');

// 创建应用实例
const app = new Koa();
// 挂载中间件
app.use(KoaErrorHandler())
app.use(KoaLogger());
app.use(koaBody({
    multipart: true,
    formidable: {
        uploadDir: './cache',
        keepExtensions: true
    }
}));
app.use(koaCompress());
// 挂载路由
app.use(router.routes());
// 启动应用
app.listen(config.port, () => {
    console.log(`Server running on http://172.28.79.234:${config.port}`);
});
