import { WebSocket, WebSocketServer } from "ws";
import config from "../common/config";

// 启动WebSocket服务
export const wss: WebSocketServer = new WebSocket.Server({ port: config.wsPort });
console.log(`WebSocket server started on port ${config.wsPort}`);
