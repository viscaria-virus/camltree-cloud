import { WorkflowProcessor } from "../ports/core";
import { StateManager } from "../ports/infra";

// 全局状态管理器
export class GlobalStore implements StateManager {
    // 类属性挂载单例
    private static readonly store: StateManager = new GlobalStore();
    // 工作流处理器集合
    private processors: Map<string, WorkflowProcessor>;
    // 私有化构造函数防止外部实例化
    private constructor() {
        this.processors = new Map();
    }
    // 类方法获取单例
    static get instance(): StateManager {
        return this.store;
    }
    // 获取状态
    public getState(key: string): WorkflowProcessor | undefined {
        return this.processors.get(key)
    }
    // 设置状态
    public setState(key: string, value: WorkflowProcessor): void {
        this.processors.set(key, value)
    }
}
