import { UniqueIDGenerator } from '../ports/infra';

// 时间戳ID生成器
export class TimeStampIDGenerator implements UniqueIDGenerator {
    // 计数器
    private count: bigint;
    // 类属性挂载单例
    private static readonly counter: UniqueIDGenerator = new TimeStampIDGenerator();
    // 构造函数设置为私有不允许外部实例化
    private constructor() {
        // 初始化计数器
        this.count = BigInt(Date.now());
    }
    // 类方法获取单例
    static get instance(): UniqueIDGenerator {
        return this.counter;
    }
    // 获取下一个ID
    public nextID(): string {
        // 自增计数器
        return String(this.count++);
    }
}
