import path from 'path';
import Config from './config.json';
import fs from 'fs';

const config = Config as any;
// 当配置中路径为相对路径时，挂载到服务进程当前工作目录下
for (const [key, value] of Object.entries(config)) {
    if (typeof value === 'string') {
        if (!path.isAbsolute(value)) {
            config[key] = path.join(process.cwd(), config[key]);
        }
    }
}
// 当工作目录不存在时进行创建
if (!fs.existsSync(config.workdir)) {
    fs.mkdirSync(config.workdir, { recursive: true });
}

export default config;
