import { enumerateCombinations } from "./utils";

// 添加点作为前缀
const addDotPrefix = (item: string) => '.' + item;
// 枚举生成所有可能的核酸/蛋白序列文件扩展名
export const sequenceSuffix: string[] = enumerateCombinations('fasta').map(addDotPrefix);
// 枚举三种特定格式的对齐序列文件扩展名
const extraSuffix: string[] = ['mega', 'nexus', 'phylip'].flatMap((item: string) => enumerateCombinations(item)).map(addDotPrefix);
// 生成所有对齐序列文件扩展名
export const alignmentSuffix: string[] = sequenceSuffix.concat(extraSuffix).concat('aln').map(addDotPrefix);
// 所有进化树文件扩展名
export const phylotreeSuffix: string[] = ['newick', 'nwk', 'treefile', 'tree', 'tre'].map(addDotPrefix);

// 任务标识编码表
export enum TaskCode {
    ALTER,
    MAFFT,
    MACSE,
    TRIMAL,
    MODEL_FINDER,
    SEQUENCE_CONCATENATOR,
    IQTREE,
    MRBAYES,
    ASTRAL
}
