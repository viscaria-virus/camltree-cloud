// 枚举字符串中字符组合
export function enumerateCombinations(source: string): string[] {
    // 字符串本身为合法结果
    const results: string[] = [source];
    // 枚举第二位字符
    for (let i = 1; i < source.length - 1; i++) {
        // 第一位字符永远是原字符串的首字符
        const characters: string[] = [source[0]];
        characters.push(source[i]);
        // 从第二位字符往后枚举第三位字符
        for (let j = i + 1; j < source.length; j++) {
            const character: string = source[j];
            // 当第三位字符与第二位字符相等时，结果不包含第三位字符
            if (character === characters[characters.length - 1]) {
                const result: string = characters.join('');
                // 将未枚举到的结果加入结果集
                if (!results.includes(result)) {
                    results.push(result);
                }
            }
            // 当第三位字符与第二位字符不相等时，结果包含第三位字符
            characters.push(character);
            const result: string = characters.join('');
            // 将未枚举到的结果加入结果集
            if (!results.includes(result)) {
                results.push(result);
            }
            // 弹出第三位字符，回溯到第二位字符继续枚举
            characters.pop();
        }
    }
    return results;
}
