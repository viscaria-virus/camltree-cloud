import { ReadStream } from "fs";
import { Workflow, WorkflowProgress, WorkflowStatusChangeEvent } from "./models";

// 工作流处理器接口
export interface WorkflowProcessor {
    // 执行工作流
    execute(workflow: Workflow): Promise<Error | null>
    // 获取工作流ID
    get workflowID(): string
    // 获取工作流进度
    get workflowProgress(): WorkflowProgress
    // 响应工作流状态变化事件
    handleStatusChange(event: WorkflowStatusChangeEvent, callback: (...args: any[]) => void): void
}

// 工作流校验器接口
export interface WorkflowValidator {
    // 校验工作流
    validate(workflow: Workflow): Error | null
}

// 工作流服务接口
export interface WorkflowService {
    // 获取工作流处理器
    getProcessor(): WorkflowProcessor;
    // 校验工作流
    verifyWorkflow(workflow: Workflow): Error | null;
    // 执行工作流
    executeWorkflow(processor: WorkflowProcessor, workflow: Workflow): string;
    // 获取指定工作流处理器
    getWorkflowProcessor(workflowID: string): WorkflowProcessor | null;
    // 获取指定工作流进度
    getWorkflowProgress(workflowID: string): WorkflowProgress | null;
}

// 文件服务接口
export interface FileService {
    // 上传文件
    uploadFile(filePath: string, content: ReadStream): Error | null;
    // 下载文件
    downloadFile(filePath: string): ReadStream;
    // 解析文件路径
    parseFilePath(filePath: string): string;
}
