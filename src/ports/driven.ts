import { ReadStream } from "fs";
import { TaskStatus } from "./models";

// 命令行工具接口
export interface CmdTool {
    // 获取命令接口
    get command(): string,
    // 获取任务状态接口
    get taskStatus(): TaskStatus,
    // 获取日志文件路径接口
    get logFilePath(): string,
    // 执行任务接口
    executeTask(inputFilePath: string, outputFilePath: string, parameters: string[]): void
}

// 文件存储模块接口
export interface FileStorage {
    // 保存文件接口
    save(filtPath: string, content: ReadStream): Error | null,
    // 读取文件接口
    load(filePath: string): ReadStream,
    // 删除文件接口
    delete(filePath: string): Error | null
    // 获取文件真实路径
    getRealPath(filePath: string): string
}
