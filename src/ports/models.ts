import Joi from "joi"
import { TaskCode } from "../common/constants"

// 任务状态枚举
export enum TaskStatus {
    // 任务准备就绪
    READY,
    // 任务正在运行
    RUNNING,
    // 任务执行成功
    SUCCESS,
    // 任务执行失败
    FAILURE
}

// 任务状态变更事件枚举
export enum TaskStatusChangeEvent {
    // 任务被拒绝
    REFUSE = 'refuse',
    // 任务开始
    BEGIN = 'begin',
    // 任务结束
    FINISH = 'finish'
}

// 任务执行结果消息模型
export interface TaskResult {
    // 任务名称
    readonly name: string,
    // 任务是否执行
    readonly execute: boolean,
    // 任务是否成功
    readonly success: boolean,
    // 错误信息
    readonly message: string,
    // 日志文件路径
    readonly log: string
}

// 任务对象模型
export interface Task {
    // 任务标识编码
    readonly code: TaskCode,
    // 任务运行参数
    arguments: string
}

// 工作流对象模型
export interface Workflow {
    // 输入文件路径
    readonly entries: string[],
    // 任务列表
    readonly tasks: Task[]
}

// 步骤状态枚举
export enum StepStatus {
    // 成功
    SUCCESS,
    // 失败
    FAILURE,
    // 运行中
    RUNNING,
    // 等待中
    WAITING
}

// 步骤进度模型
export interface StepProgress {
    // 任务标识编码
    readonly code: bigint,
    // 已完成子任务数
    finish: bigint,
    // 子任务总数
    readonly total: bigint,
    // 步骤状态
    status: StepStatus
}

// 工作流状态枚举
export enum WorkflowStatus {
    // 成功
    SUCCESS,
    // 待定
    PENDING,
    // 失败
    FAILURE,
}

// 工作流进度模型
export interface WorkflowProgress {
    // 步骤进度列表
    readonly progress: StepProgress[],
    // 工作流状态
    status: WorkflowStatus
}

// 错误响应模型
export interface ErrorResponse {
    // 错误原因
    cause: string,
    // 错误细节
    detail: string,
    // 解决方案
    solution: string
}

// 工作流状态变更事件枚举
export enum WorkflowStatusChangeEvent {
    // 进度更新
    UPDATE = 'update',
    // 执行结束
    FINISH = 'finish'
}

// 请求参数校验模板
export const RequestSchemas = {
    // 以workflowID为参数发送的GET请求
    GetByWorkflowID: Joi.object({
        // id是必需字段，且为数值
        id: Joi.number().required(),
    }),
    // 上传工作流的POST请求
    PostWorkflow: Joi.object({
        // tasks是必需字段，且为数组，其元素为对象
        tasks: Joi.array().required().items(Joi.object({
            // code是必需字段，且为数值，且必须为TaskCode枚举中的值
            code: Joi.number().required().valid(...Object.values(TaskCode)),
            // arguments是必须字段，且为字符串，允许为空字符串
            arguments: Joi.string().required().allow(''),
        })),
        // entries是必须字段，且为非空数组，其元素为字符串
        entries: Joi.array().required().items(Joi.string()),
    })
};
