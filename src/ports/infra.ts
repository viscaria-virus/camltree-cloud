// 唯一ID生成器接口
export interface UniqueIDGenerator {
    // 获取下一个唯一ID
    nextID(): string
}

// 状态键类型
export type StateKey = string | number | symbol

// 状态管理器接口
export interface StateManager {
    // 设置状态
    setState(key: StateKey, value: any): void
    // 获取状态
    getState(key: StateKey): any
}
